# Console Overhaul
## id: console
Add the exclusive features from Minecraft: Console Edition and some from Minecraft: Bedrock Edition!.
Visit @mortada_killer0, @hello_, @indianajmh

## If Your Packs is not Working
Try This
```
{
	"pack": {
		"pack_format": 5,	// The Format That Changes How Pack Work 
		"name": "Example Pack",	// Call Your Pack with Any Name You Want
		"description": "Very Very Very Long Example Pack Description to Type in What Your Pack is About or What the Pack Have in One Line",	// Type Anything You Want There
		"type": "resources",	// Valid Values: resources, data, skins, template
		"uuid": "00000000-0000-0000-000000000000",	// Fill it With Random Numbers and Letters (0-9, a-f)
	},
	"language": {},	// if You Created a Custom Language, Put it Here
	"meta": {
		"game_version": 1.15.2,	// The Version of the Game the Pack Work in it
	}
}
```